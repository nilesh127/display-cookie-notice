window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#000"
    },
    "button": {
      "background": "transparent",
      "border": "#ffffff",
      "text": "#ffffff"
    }
  },
  "position": "bottom-right",
  "content": {
    "link": "Privacy Policy",
    "href": "https://athflex.com/privacy-policy/"
  }
})});